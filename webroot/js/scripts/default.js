var btnCollapse = document.getElementsByClassName('collapse-toggle');
for(var x = 0; x < btnCollapse.length; ++x) {
  btnCollapse[x].addEventListener('click', handleCollapse, false);
}

function handleCollapse() {
  var attrId = this.getAttribute('id');
  var child = this.childNodes[0];
  var className = child.getAttribute('class');
  // muda o ícone do botão
  if(className.match(/o-down/)) {
    child.setAttribute('class', 'fa fa-caret-square-o-up')
  } else {
    child.setAttribute('class', 'fa fa-caret-square-o-down')
  }
  // show/hide desired element
  $(attrId).toggleClass('open');
}
