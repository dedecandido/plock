<?php
$this->Html->addCrumb('Listagem', null);
?>
<div class="btn-toolbar">
	<?= $this->Html->link('<i class="fa fa-plus"></i> adicionar', [ 'action' => 'add' ], [ 'class' => 'btn btn-xs btn-primary', 'escape' => false ]); ?>
</div>
<?php foreach($clients as $client): ?>
<!-- <article class="col-sm-4  js-masonry" data-masonry-options='{ "itemSelector": ".col-sm-4", "columnWidth": "" }'> -->
<article class="col-sm-4">
	<div class="panel clientes">
		<div class="panel-body">
			<h3><?= $client['Client']['name']; ?></h3>
			<ul>
				<li><span class="fa fa-envelope-o fa-fw fa-1"></span> <?= $client['Client']['email']; ?></li>
				<li><span class="fa fa-phone fa-fw fa-1"></span> <?= $client['Client']['phone']; ?></li>
				<li><span class="fa fa-globe fa-fw fa-1"></span> <?= $client['Client']['website']; ?></li>
			</ul>

			<?php if(sizeof($client['Account']) > 0): ?>
				<h4>
					CONEXÕES (<?= sizeof($client['Account']) ?>)
					<button class="collapse-toggle" id="#collapse<?= $client['Client']['id']; ?>"><span class="fa fa-caret-square-o-down"></span></button>
				</h4>
			<?php endif; ?>
		</div>
		<div class="panel-footer collapse" id="collapse<?= $client['Client']['id']; ?>">
			<?php
				if(sizeof($client['Account']) > 0):
					$categoria = null;
					echo '<ul>';
					foreach($client['Account'] as $key => $account):
						if($account['Category']['name'] != $categoria):
							$categoria = $account['Category']['name'];
							echo '<li class="nav-header">'. $categoria .'</li>';
						endif;
						echo '<li><span class="fa fa-globe fa-fw fa-1"></span>'. $account['host'] .'</li>';
						echo '<li><span class="fa fa-user fa-fw fa-1"></span>'. $account['username'] .'</li>';
						echo '<li><span class="fa fa-unlock-alt fa-fw fa-1"></span>'. $account['password'] .'</li>';
						echo isset($client['Account'][$key + 1]) ? '<li class="divider"></li>' : '';
					endforeach;
					echo '</ul>';
				endif;
			?>
		</div>
	</div>
</article>
<?php endforeach; ?>
