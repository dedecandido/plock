<?php 
$this->Html->addCrumb($this->request->data('Client.name'), null);
?>
<div class="page-header">
	<h1><?php echo $this->Html->link('Clients', ['action' => 'index']); ?><span>&raquo;</span></h1>
	<?php echo $this->Html->getCrumbList( [ 'class' => 'breadcrumb' ], false); ?>
</div>

<?php 
echo $this->Session->flash();
echo $this->Form->create('Client', [
	'class' => 'form-horizontal',
	'inputDefaults' => [
		'class' => 'form-control input-sm',
		'div' => 'col-sm-5',
		'label' => false
	]
]); 
echo $this->Form->input('id');
?>
<div class="form-group">
	<?php echo $this->Form->label('name', 'Name', 'control-label col-sm-1'); ?>
	<?php echo $this->Form->input('name'); ?>
</div>
<div class="form-group">
	<?php echo $this->Form->label('phone', 'Phone', 'control-label col-sm-1'); ?>
	<?php echo $this->Form->input('phone'); ?>
</div>
<div class="form-group">
	<?php echo $this->Form->label('email', 'Email', 'control-label col-sm-1'); ?>
	<?php echo $this->Form->input('email'); ?>
</div>
<div class="form-group">
	<?php echo $this->Form->label('website', 'Website', 'control-label col-sm-1'); ?>
	<?php echo $this->Form->input('website'); ?>
</div>
<div class="form-group">
	<div class="col-sm-5 col-sm-offset-1">
		<?php echo $this->Form->button('<span class="fa fa-save"></span> Save client', ['class' => 'btn btn-primary btn-sm', 'type' => 'submit']); ?>
	</div>
</div>
<?php echo $this->Form->end(); ?>