<!DOCTYPE html>
<html lang="pt">
	<head>
		<?php echo $this->Html->charset(); ?>
		<?php echo $this->fetch('meta'); ?>
		<title><?php echo $this->fetch('title'); ?> - Gerenciador de Contas</title>
		<?php
			echo $this->Html->meta('icon');
			echo $this->Html->css(['font-awesome.min', 'default']);
		?>
	</head>
	<body>
		<section class="container">
			<div class="row">
				<nav class="aside" role="navigation">
					<?php echo $this->Html->image('stalo-comunicacao.png', [ 'class' => 'logo img-responsive', 'alt' => 'Stalo Comunicação' ]); ?>

					<ul>
						<li><?php echo $this->Html->link('<i class="fa fa-tags fa-fw"></i> Categorias', [ 'controller' => 'categories', 'action' => 'index' ], [ 'escape' => false ]); ?></li>
						<li><?php echo $this->Html->link('<i class="fa fa-user fa-fw"></i> Clientes', [ 'controller' => 'clients', 'action' => 'index' ], [ 'escape' => false ]); ?></li>
						<li><?php echo $this->Html->link('<i class="fa fa-folder fa-fw"></i> Contas', [ 'controller' => 'accounts', 'action' => 'index' ], [ 'escape' => false ]); ?></li>
					</ul>
				</nav>
				<main class="content" role="main">
					<div class="top-bar">
						<h1 class="page-header">
							<?= $title_for_layout; ?>
							<?= $this->Html->getCrumbList(['class' => 'breadcrumb'], false); ?>
						</h1>
					</div>
					<?php echo $this->fetch('content'); ?>
				</main>
			</div>
		</section>
		<?php echo $this->Html->script([ 'src/jquery-2.1.4.min', 'src/masonry.pkgd.min', 'scripts/default' ]); ?>
	</body>
</html>
