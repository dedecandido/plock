<div class="page-header">
	<h1>Accounts</h1>	
</div>

<div class="btn-toolbar">
	<?php echo $this->Html->link('<i class="fa fa-plus"></i> Adicionar', [ 'action' => 'add' ], [ 'class' => 'btn btn-sm btn-default', 'escape' => false ]); ?>
</div>

<?php echo $this->Session->flash(); ?>

<table class="table table-striped">
	<thead>
		<tr>			
			<th><?php echo $this->Paginator->sort('client_id'); ?></th>
			<!--th><?php echo $this->Paginator->sort('category_id'); ?></th-->
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('host'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('password'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($accounts as $account): ?>
			<tr>				
				<td>
					<?php echo $this->Html->link($account['Client']['name'], array('controller' => 'clients', 'action' => 'view', $account['Client']['id'])); ?>
				</td>
				<!--td>
					<?php echo $this->Html->link($account['Category']['name'], array('controller' => 'categories', 'action' => 'view', $account['Category']['id'])); ?>
				</td-->
				<td><?php echo h($account['Account']['name']); ?>&nbsp;</td>
				<td><?php echo h($account['Account']['host']); ?>&nbsp;</td>
				<td><?php echo h($account['Account']['username']); ?>&nbsp;</td>
				<td><?php echo h($account['Account']['password']); ?>&nbsp;</td>
				<td class="actions">
					<?php
						if($account['Category']['name'] == 'Database') {
							if(strpos($account['Account']['host'], 'stalo') > 0) {
								echo $this->Form->create(null, [
									'id' => 'Database' . $account['Account']['id'],
									'url' => 'http://phpmyadmin.locaweb.com.br/', 
									'target' => '_blank'
								]);
								echo $this->Form->input('pma_servername', [ 'name' => 'pma_servername', 'value' => $account['Account']['host'], 'type' => 'hidden' ]);
								echo $this->Form->input('pma_username', [ 'name' => 'pma_username', 'value' => $account['Account']['username'], 'type' => 'hidden' ]);
								echo $this->Form->input('pma_password', [ 'name' => 'pma_password', 'value' => $account['Account']['password'], 'type' => 'hidden' ]);							
								echo $this->Form->end();
								echo $this->Html->link('<i class="fa fa-sign-in"></i>', '', [ 'escape' => false, 'onclick' => '$(\'form#Database' . $account['Account']['id'] .'\').submit(); return false' ]);
							} else {
								echo $this->Html->link('<i class="fa fa-sign-in"></i>', $account['Account']['host'], ['escape' => false, 'target' => '_blank']);
							}
						}
					?>					
					<?php echo $this->Html->link('<i class="fa fa-edit"></i>',[ 'action' => 'edit', $account['Account']['id'] ], [ 'escape' => false ]); ?>
					<?php echo $this->Form->postLink('<i class="fa fa-trash"></i>', [ 'action' => 'delete', $account['Account']['id'] ], [ 'escape' => false ], [ 'confirm' => __('Are you sure you want to delete # %s?', $account['Account']['id']) ]); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>