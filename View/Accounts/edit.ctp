<?php
$this->Html->addCrumb('Accounts', [ 'action' => 'index'] );
$this->Html->addCrumb('edit', null);
?>
<div class="page-header">
	<h1>Accounts <span>&raquo;</span></h1>
	<?php echo $this->Html->getCrumbList([ 'class' => 'breadcrumb' ], false); ?>
</div>

<?php 
echo $this->Session->flash();
echo $this->Form->create('Account', [ 
	'class' => 'form-horizontal', 
	'inputDefaults' => [ 
		'class' => 'form-control input-sm', 
		'div' => 'col-sm-5', 
		'label' => false 
	] 
]); 
echo $this->Form->input('id');
?>
	<div class="form-group">
		<?php echo $this->Form->label('client_id', 'Client', 'control-label col-sm-1'); ?>
		<?php echo $this->Form->input('client_id', [ 'empty' => '' ]); ?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->label('category_id', 'Category', 'control-label col-sm-1'); ?>
		<?php echo $this->Form->input('category_id', [ 'empty' => '' ]); ?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->label('name', 'Name', 'control-label col-sm-1'); ?>
		<?php echo $this->Form->input('name'); ?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->label('host', 'Host', 'control-label col-sm-1'); ?>
		<?php echo $this->Form->input('host'); ?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->label('username', 'Username', 'control-label col-sm-1'); ?>
		<?php echo $this->Form->input('username'); ?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->label('password', 'Password', 'control-label col-sm-1'); ?>
		<?php echo $this->Form->input('password', [ 'type' => 'text' ]); ?>
	</div>
	<div class="form-group">
		<div class="col-sm-5 col-sm-offset-1"><?php echo $this->Form->button('<span class="fa fa-save"></span> Save changes', [ 'class' => 'btn btn-primary btn-sm', 'type' => 'submit' ]); ?></div>
	</div>
<?php echo $this->Form->end(); ?>